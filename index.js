(function main() {
  let anim;
  
  document.getElementById('fadeInPlay')
    .addEventListener('click', function () {
      const block = document.getElementById('fadeInBlock');
      animaster().fadeIn(block, 5000);
    });
  document.getElementById('fadeInReset')
    .addEventListener('click', function () {
      const block = document.getElementById('fadeInBlock');
      resetFadeIn(block);
    });
  
  document.getElementById('movePlay')
    .addEventListener('click', function () {
      const block = document.getElementById('moveBlock');
      animaster().move(block, 1000, {x: 100, y: 10});
    });
  document.getElementById('moveReset')
    .addEventListener('click', function () {
      const block = document.getElementById('moveBlock');
      resetMoveAndScale(block);
    });
  
  document.getElementById('scalePlay')
    .addEventListener('click', function () {
      const block = document.getElementById('scaleBlock');
      animaster().scale(block, 1000, 1.25);
    });
  document.getElementById('scaleReset')
    .addEventListener('click', function () {
      const block = document.getElementById('scaleBlock');
      resetMoveAndScale(block);
    });
  
  document.getElementById('fadeOutPlay')
    .addEventListener('click', function () {
      const block = document.getElementById('fadeOutBlock');
      animaster().fadeOut(block, 5000);
    });
  document.getElementById('fadeOutReset')
    .addEventListener('click', function () {
      const block = document.getElementById('fadeOutBlock');
      resetFadeOut(block);
    });
  
  document.getElementById('heartBeatingPlay')
    .addEventListener('click', function () {
      const block = document.getElementById('heartBeatingBlock');
      anim = animaster().heartBeating(block, 500);
      document.getElementById('heartBeatingStop')
        .addEventListener('click', function () {
          anim.stop();
        });
    });
  
  document.getElementById('shakingPlay')
    .addEventListener('click', function () {
      const block = document.getElementById('shakingBlock');
      anim = animaster().shaking(block, 500);
      document.getElementById('shakingStop')
        .addEventListener('click', function () {
          anim.stop();
          anim.reset();
        });
    });
  
  document.getElementById('moveAndHidePlay')
    .addEventListener('click', function () {
      const block = document.getElementById('moveAndHideBlock');
      animaster().moveAndHide(block, 5000, {x: 100, y: 20});
    });
  document.getElementById('moveAndHideReset')
    .addEventListener('click', function () {
      const block = document.getElementById('moveAndHideBlock');
      resetFadeOut(block);
      resetMoveAndScale(block);
    });
  
  document.getElementById('showAndHidePlay')
    .addEventListener('click', function () {
      const block = document.getElementById('showAndHideBlock');
      animaster().showAndHide(block, 5000);
    });
  
})();

function animaster() {
  return {
    _steps: [],
    _translation: {x: 0, y: 0},
    _ratio: 1,
    scale: function (element, duration, ratio) {
      this._ratio  = ratio;
      element.style.transitionDuration = `${duration}ms`;
      element.style.transform = getTransform(this._translation, this._ratio);
      console.log(element.style.transform);
  
      return{
        reset : () => {
          resetMoveAndScale(element)
        }
      }
    },
    move: function (element, duration, translation) {
      this._translation = translation;
      element.style.transitionDuration = `${duration}ms`;
      element.style.transform = getTransform(this._translation, this._ratio);
      console.log(element.style.transform);
      
      return{
        reset : () => {
          resetMoveAndScale(element)
        }
      }
    },
    fadeIn: function (element, duration) {
      element.style.transitionDuration = `${duration}ms`;
      element.classList.remove('hide');
      element.classList.add('show');
      
      return {
        reset: function () {
          resetFadeIn(element)
        }
      }
    },
    fadeOut: function (element, duration) {
      element.style.transitionDuration = `${duration}ms`;
      element.classList.remove("show");
      element.classList.add("hide");
      
      return {
        reset: function () {
          resetFadeOut(element)
        }
      }
    },
    moveAndHide: function (element, duration, translation) {
      return this.addMove(duration * 0.4, translation).addFadeOut(duration * 0.6).play(element);
    },
    showAndHide: function (element, duration) {
      this.name = 'showAndHide';
      return this.addFadeIn(duration / 3).addDelay(duration / 3).addFadeOut(duration / 3).play(element);
    },
    heartBeating: function (element, duration) {
      return this.addScale(duration, 1.4).addScale(duration, 1).play(element, true);
    },
    shaking: function (element, duration) {
      return this.addMove(duration, {x: 20, y: 0}).addMove(duration, {x: 0, y: 0}).play(element, true);
    },
    addMove: function (duration, translation) {
      const anim = {name: "move", duration: duration, translation: translation};
      this._steps.push(anim);
      return this;
    },
    addScale: function (duration, ratio) {
      const anim = {name: "scale", duration: duration, ratio: ratio};
      this._steps.push(anim);
      return this;
    },
    addFadeIn: function (duration) {
      const anim = {name: "fadeIn", duration: duration};
      this._steps.push(anim);
      return this;
    },
    addFadeOut: function (duration) {
      const anim = {name: "fadeOut", duration: duration, translation: null, ratio: null};
      this._steps.push(anim);
      return this;
    },
    addDelay: function (duration) {
      setTimeout(() => {
      }, duration);
      return this;
    },
    buildHandler: function (cycled) {
      let that = this;
      return function () {
        that.play(this, cycled);
      }
    },
    play: function (element, cycled = false) {
      let that = this;
      let animHandle;
      let _reset = [];
  
  
      function playAnim(index, cycled) {
        const {duration, name, translation, ratio} = {...that._steps[index]};
        switch (name) {
          case "scale":
            _reset.push(animaster().scale.call(that, element, duration, ratio));
            break;
          case "move":
            _reset.push(animaster().move.call(that, element, duration, translation));
            break;
          case "fadeIn":
            _reset.push(animaster().fadeIn.call(that, element,duration));
            break;
          case "fadeOut":
            _reset.push(animaster().fadeOut.call(that, element, duration));
            break;
          case "delay":
            break;
          default:
            console.log("Animation not found");
            break;
        }
        
        animHandle = setTimeout(() => {
          if (index + 1 < that._steps.length) {
            playAnim(index + 1, cycled)
          } else if (cycled) {
            playAnim(0, cycled);
          }
        }, duration);
      }
      
      playAnim(0, cycled);
      
      return{
        stop: () => {
          clearInterval(animHandle)
        },
        reset: () => {
          for(let i = 0; i < _reset.length; i++)
            _reset[i].reset();
        }
      }
    }
  };
}

function getTransform(translation, ratio) {
  const result = [];
  if (translation) {
    result.push(`translate(${translation.x}px,${translation.y}px)`);
  }
  if (ratio) {
    result.push(`scale(${ratio})`);
  }
  return result.join(' ');
}

function resetFadeIn(element) {
  element.style.transitionDuration = null;
  element.classList.remove("show");
  element.classList.add("hide");
}

function resetFadeOut(element) {
  element.style.transitionDuration = null;
  element.classList.remove("hide");
  element.classList.add("show");
}

function resetMoveAndScale(element) {
  element.style.transitionDuration = null;
  element.style.transform = null;
}
